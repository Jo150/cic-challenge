# README #

CIC-Challenge

Contains front-end source files as well as back end source files.

### Functionality - Backend###
#### Request all movies and locations####
https://cic-challenge-peitl.mybluemix.net/rest/MLService

#### Filter response ####
'https://cic-challenge-peitl.mybluemix.net/rest/MLService/{search_phrase}'

Example:
https://cic-challenge-peitl.mybluemix.net/rest/MLService/180


### Functionality - Frontend###

The app lists all locations from movies shot in San Francisco.

https://cic-challenge-website.mybluemix.net

* search for a specific movie
* expand a movie to list all locations
package services;

 
/**
 * @author Johannes Peitl
 */
 
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.json.JSONObject;
import org.json.JSONArray;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
 
@Path("/MLService")
public class MLService {
	
	private final String sURL = "http://data.sfgov.org/resource/wwmu-gmzc.json?$select=locations,title";
	
	/**
	 * Get list of locations of movies in San Francisco in JSON object.
	 * @return
	 */
	private JSONArray getData() {
		try {
			URL url = new URL(sURL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
	
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
	
			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
	
			String line;
			String json = "";
			while ((line = br.readLine()) != null) {
				json+=line;
			}
			
			conn.disconnect();
			return new JSONArray(json);
		}
		catch(MalformedURLException e) {

			e.printStackTrace();

		  } catch (IOException e) {

			e.printStackTrace();

		  }
		return new JSONArray();
	}
	
	@GET
	@Produces("application/json")
	public Response getMoviesLocations() {
		return Response.status(200).entity(getData().toString()).build();
	}
 
	@Path("{m}")
	@GET
	@Produces("application/json")
	public Response getMoviesLocations(@PathParam("m") String m) {
		JSONArray jsonArray = getData();
		JSONArray filteredArray = new JSONArray();
		//Filter rows where the title does not contain the search phrase.
		for(int i = 0; i < jsonArray.length(); i++) {
			String title = ((JSONObject) jsonArray.get(i)).get("title").toString();
		    if(title.toLowerCase().contains(m.toLowerCase())) {
		    	filteredArray.put(jsonArray.get(i));
		    }
		}
		return Response.status(200).entity(filteredArray.toString()).build();
	}
}
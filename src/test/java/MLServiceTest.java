 import io.restassured.*;
 import static io.restassured.RestAssured.given;

 import org.junit.*;

public class MLServiceTest {
	
	@BeforeClass
    public static void setup() {
        String port = System.getProperty("server.port");
        if (port == null) {
            RestAssured.port = Integer.valueOf(8080);
        }
        else{
            RestAssured.port = Integer.valueOf(port);
        }


        String basePath = System.getProperty("server.base");
        if(basePath==null){
            basePath = "/cic-challenge-service/";
        }
        RestAssured.basePath = basePath;

        String baseHost = System.getProperty("server.host");
        if(baseHost==null){
            baseHost = "http://localhost";
        }
        RestAssured.baseURI = baseHost;

    }
	@Test 
	public void testGetMoviesLocationsResponse()  {
		given().when().get("/MLService")
        .then().statusCode(200);
	}
	@Test 
	public void testGetMoviesLocationsFilterResponse()  {
		given().when().get("/MLService/180")
        .then().statusCode(200);
	}

}
function createCollection(data) {
	var films = {};
	data.forEach(function(element) {
			if (!(element.title in films)) {
				films[element.title] = {name:element.title, childs:[]};
			}
			films[element.title].childs.push(element.locations);
	});
	console.log(films);
	return films;
};
angular.module('challengeApp', [])
  .controller('FilmsCtrl', function($scope, $http){
		      $http.get('rows.xml')
			          .success(function (response) {
							  console.log(response);
							$scope.items = createCollection(response.response.row.row);
							$scope.itemsSaved = angular.copy($scope.items);
					  });
		  	   $scope.change = function() {
				  var searchTerm = $scope.search; 
				  var tmpItems = angular.copy($scope.itemsSaved);
				  for (var key in tmpItems) {
						if(!(tmpItems[key].name.indexOf(searchTerm) >= 0)) {
							delete tmpItems[key];
						}
				  }
				  $scope.items = tmpItems;
		  		};

			  
  });
